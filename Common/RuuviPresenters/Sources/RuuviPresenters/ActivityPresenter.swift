import Foundation

public protocol ActivityPresenter {
    func increment()
    func decrement()
}
