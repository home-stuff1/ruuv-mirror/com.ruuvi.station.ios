import Foundation
import RuuviOntology
import RuuviLocal

// swiftlint:disable:next type_body_length
final class RuuviLocalSettingsUserDefaults: RuuviLocalSettings {

    private let keepConnectionDialogWasShownUDPrefix = "SettingsUserDegaults.keepConnectionDialogWasShownUDPrefix."

    func keepConnectionDialogWasShown(for luid: LocalIdentifier) -> Bool {
        return UserDefaults.standard.bool(forKey: keepConnectionDialogWasShownUDPrefix + luid.value)
    }

    func setKeepConnectionDialogWasShown(for luid: LocalIdentifier) {
        UserDefaults.standard.set(true, forKey: keepConnectionDialogWasShownUDPrefix + luid.value)
    }

    private let firmwareUpdateDialogWasShownUDPrefix = "SettingsUserDegaults.firmwareUpdateDialogWasShownUDPrefix."

    func firmwareUpdateDialogWasShown(for luid: LocalIdentifier) -> Bool {
        return UserDefaults.standard.bool(forKey: firmwareUpdateDialogWasShownUDPrefix + luid.value)
    }

    func setFirmwareUpdateDialogWasShown(for luid: LocalIdentifier) {
        UserDefaults.standard.set(true, forKey: firmwareUpdateDialogWasShownUDPrefix + luid.value)
    }

    private let firmwareVersionPrefix = "SettingsUserDegaults.firmwareVersionPrefix"
    func firmwareVersion(for luid: LocalIdentifier) -> String? {
        return UserDefaults.standard.value(forKey: firmwareVersionPrefix + luid.value) as? String
    }

    func setFirmwareVersion(for luid: LocalIdentifier, value: String) {
        UserDefaults.standard.set(value, forKey: firmwareVersionPrefix + luid.value)
    }

    func removeFirmwareVersion(for luid: LocalIdentifier) {
        UserDefaults.standard.removeObject(forKey: firmwareVersionPrefix + luid.value)
    }

    // Store Chart Foreground State
    private let chartOnForegroundPrefix = "SettingsUserDefaults.chartOnForeground"
    func tagChartOnForeground(for luid: LocalIdentifier) -> Bool {
        return UserDefaults.standard.value(forKey: chartOnForegroundPrefix + luid.value) as? Bool ?? false
    }

    func setTagChartOnForeground(for luid: LocalIdentifier, value: Bool) {
        UserDefaults.standard.set(value, forKey: chartOnForegroundPrefix + luid.value)
    }

    var language: Language {
        get {
            if let savedCode = UserDefaults.standard.string(forKey: languageUDKey) {
                return Language(rawValue: savedCode) ?? .english
            } else if let regionCode = Locale.current.languageCode {
                return Language(rawValue: regionCode) ?? .english
            } else {
                return .english
            }
        }
        set {
            UserDefaults.standard.set(newValue.rawValue, forKey: languageUDKey)
            NotificationCenter
                .default
                .post(name: .LanguageDidChange,
                      object: self,
                      userInfo: nil)
        }
    }
    private let languageUDKey = "SettingsUserDegaults.languageUDKey"

    var humidityUnit: HumidityUnit {
        get {
            switch humidityUnitInt {
            case 1:
                return .gm3
            case 2:
                return .dew
            default:
                return .percent
            }
        }
        set {
            switch newValue {
            case .percent:
                humidityUnitInt = 0
            case .gm3:
                humidityUnitInt = 1
            case .dew:
                humidityUnitInt = 2
            }
            NotificationCenter
                .default
                .post(name: .HumidityUnitDidChange,
                      object: self,
                      userInfo: nil)
        }
    }

    var temperatureUnit: TemperatureUnit {
        get {
            switch temperatureUnitInt {
            case 0:
                return useFahrenheit ? .fahrenheit : .celsius
            case 1:
                return .kelvin
            case 2:
                return .celsius
            case 3:
                return .fahrenheit
            default:
                return .celsius
            }
        }
        set {
            useFahrenheit = newValue == .fahrenheit
            switch newValue {
            case .kelvin:
                temperatureUnitInt = 1
            case .celsius:
                temperatureUnitInt = 2
            case .fahrenheit:
                temperatureUnitInt = 3
            }
            NotificationCenter
                .default
                .post(name: .TemperatureUnitDidChange,
                      object: self,
                      userInfo: nil)
        }
    }

    var pressureUnit: UnitPressure {
        get {
            switch pressureUnitInt {
            case UnitPressure.inchesOfMercury.hashValue:
                return .inchesOfMercury
            case UnitPressure.millimetersOfMercury.hashValue:
                return .millimetersOfMercury
            default:
                return .hectopascals
            }
        }
        set {
            pressureUnitInt = newValue.hashValue
            NotificationCenter
                .default
                .post(name: .PressureUnitDidChange,
                      object: self,
                      userInfo: nil)
        }
    }

    @UserDefault("SettingsUserDefaults.pressureUnitInt", defaultValue: UnitPressure.hectopascals.hashValue)
    private var pressureUnitInt: Int

    @UserDefault("SettingsUserDegaults.welcomeShown", defaultValue: false)
    var welcomeShown: Bool

    @UserDefault("SettingsUserDegaults.tagChartsLandscapeSwipeInstructionWasShown", defaultValue: false)
    var tagChartsLandscapeSwipeInstructionWasShown: Bool

    @UserDefault("DashboardScrollViewController.hasShownSwipeAlert", defaultValue: false)
     var cardsSwipeHintWasShown: Bool

    @UserDefault("SettingsUserDegaults.isAdvertisementDaemonOn", defaultValue: true)
    var isAdvertisementDaemonOn: Bool {
        didSet {
            NotificationCenter
            .default
            .post(name: .isAdvertisementDaemonOnDidChange,
                  object: self,
                  userInfo: nil)
        }
    }

    @UserDefault("SettingsUserDegaults.isWebTagDaemonOn", defaultValue: true)
    var isWebTagDaemonOn: Bool {
        didSet {
            NotificationCenter
            .default
            .post(name: .isWebTagDaemonOnDidChange,
                  object: self,
                  userInfo: nil)
        }
    }

    @UserDefault("SettingsUserDegaults.webTagDaemonIntervalMinutes", defaultValue: 60)
    var webTagDaemonIntervalMinutes: Int {

        didSet {
            NotificationCenter
            .default
            .post(name: .WebTagDaemonIntervalDidChange,
             object: self,
             userInfo: nil)
        }
    }

    @UserDefault("SettingsUserDegaults.connectionTimeout", defaultValue: 30)
    var connectionTimeout: TimeInterval

    @UserDefault("SettingsUserDegaults.serviceTimeout", defaultValue: 300)
    var serviceTimeout: TimeInterval

    @UserDefault("SettingsUserDegaults.advertisementDaemonIntervalMinutes", defaultValue: 1)
    var advertisementDaemonIntervalMinutes: Int

    @UserDefault("SettingsUserDegaults.alertsMuteIntervalMinutes", defaultValue: 60)
    var alertsMuteIntervalMinutes: Int

    @UserDefault("SettingsUserDegaults.saveHeartbeats", defaultValue: true)
    var saveHeartbeats: Bool

    @UserDefault("SettingsUserDegaults.saveHeartbeatsIntervalMinutes", defaultValue: 5)
    var saveHeartbeatsIntervalMinutes: Int

    @UserDefault("SettingsUserDegaults.webPullIntervalMinutes", defaultValue: 15)
    var webPullIntervalMinutes: Int

    @UserDefault("SettingsUserDegaults.dataPruningOffsetHours", defaultValue: 240)
    var dataPruningOffsetHours: Int

    @UserDefault("SettingsUserDegaults.chartIntervalSeconds", defaultValue: 300)
    var chartIntervalSeconds: Int

    @UserDefault("SettingsUserDegaults.chartDurationHours", defaultValue: 240)
    var chartDurationHours: Int {
        didSet {
            NotificationCenter
                .default
                .post(name: .ChartDurationHourDidChange,
                      object: self,
                      userInfo: nil)
        }
    }

    @UserDefault("SettingsUserDefaults.networkPullIntervalMinutes", defaultValue: 60)
    var networkPullIntervalSeconds: Int

    @UserDefault("SettingsUserDefaults.networkPruningIntervalHours", defaultValue: 240)
    var networkPruningIntervalHours: Int

    // MARK: - Private
    @UserDefault("SettingsUserDegaults.useFahrenheit", defaultValue: false)
    private var useFahrenheit: Bool

    private var temperatureUnitInt: Int {
        get {
            let int = UserDefaults.standard.integer(forKey: temperatureUnitIntUDKey)
            if int == 0 {
                if useFahrenheit {
                    temperatureUnit = .fahrenheit
                    return 3
                } else {
                    temperatureUnit = .celsius
                    return 2
                }
            } else {
                return int
            }
        }
        set {
            UserDefaults.standard.set(newValue, forKey: temperatureUnitIntUDKey)
        }
    }
    private let temperatureUnitIntUDKey = "SettingsUserDegaults.temperatureUnitIntUDKey"

    private var humidityUnitInt: Int {
        get {
            return UserDefaults.standard.integer(forKey: humidityUnitIntUDKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: humidityUnitIntUDKey)
        }
    }
    private let humidityUnitIntUDKey = "SettingsUserDegaults.humidityUnitInt"

    @UserDefault("SettingsUserDefaults.chartDownsamplingOn", defaultValue: false)
    var chartDownsamplingOn: Bool {
        didSet {
            NotificationCenter
                .default
                .post(name: .DownsampleOnDidChange,
                      object: self,
                      userInfo: nil)
        }
    }

    @UserDefault("SettingsUserDefaults.chartDrawDotsOn", defaultValue: false)
    var chartDrawDotsOn: Bool {
        didSet {
            NotificationCenter
                .default
                .post(name: .ChartDrawDotsOnDidChange,
                      object: self,
                      userInfo: nil)
        }
    }

    @UserDefault("SettingsUserDefaults.experimentalFeaturesEnabled", defaultValue: false)
    var experimentalFeaturesEnabled: Bool

    @UserDefault("SettingsUserDefaults.cloudModeEnabled", defaultValue: false)
    var cloudModeEnabled: Bool {
        didSet {
            NotificationCenter
                .default
                .post(name: .CloudModeDidChange,
                      object: self,
                      userInfo: nil)
        }
    }
}
